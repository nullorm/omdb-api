const axios = require('axios')
const Router = require('koa-router')

const router = new Router({ prefix: '/omdb' })

module.exports = router

router.get('/details/:id', async ctx => {
  const { id } = ctx.params
  const uri = `http://www.omdbapi.com/?apikey=f2c4ab75&i=${id}`
  try {
    const response = await axios.get(uri)
    const data = response.data
    ctx.body = { data }
  } catch (error) {
    ctx.body = { error: { message: error.message } }
  }
})

router.get('/search', async ctx => {
  const { page, s } = ctx.query
  const uri = `http://www.omdbapi.com/?apikey=f2c4ab75&s=${s}&page=${page || 1}`
  try {
    const response = await axios.get(uri)
    const data = response.data
    ctx.body = { data }
  } catch (error) {
    ctx.body = { error: { message: error.message } }
  }
})
