const Koa = require('koa')
const cors = require('@koa/cors')
const logger = require('koa-logger')

const omdbRouter = require('./omdb')

const app = new Koa()

app.use(logger())
app.use(cors())

app.use(omdbRouter.routes())

app.listen(3001)
